use crate::server::base_dao;
use crate::util::schedule;
use crate::base;
use crate::base::MAC_ADDRESS;
use crate::util::panic_main::panic_;

pub fn timekeeping() {
    let base_file = base_dao::read_file();
    schedule::start(
        base_file.used(),
        10,
        |_| {
            let mut read_in_base_file = base_dao::read_file();
            if MAC_ADDRESS.as_str().ne(read_in_base_file.mac_address()) {
                panic_(format!(
                    "read base file mac address `{}` is not matching current `{}`",
                    read_in_base_file.mac_address(),
                    MAC_ADDRESS.as_str()
                ));
            }
            let next_time = read_in_base_file.used() + 10;
            if next_time <= read_in_base_file.total() {
                read_in_base_file.set_used(next_time);
            }
            base_dao::write_file(&read_in_base_file);
            base::current_base().exchange(read_in_base_file);
        });
}

#[test]
fn test_start() {
    timekeeping();
    loop {}
}