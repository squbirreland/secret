use std::fmt::{
    self, Display,
};

#[derive(Debug)]
pub struct ServerError {
    message: String,
}

impl Display for ServerError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "ServerError : {}", self.message)
    }
}

impl ServerError {
    pub fn new(message: String) -> Self {
        Self { message }
    }
    pub fn get_message(&self) -> String { self.message.to_string() }
}