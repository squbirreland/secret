use serde::{Serialize, Deserialize};
use chrono::Local;

#[derive(Serialize, Deserialize, Debug, Default, Clone)]
pub struct BaseFile {
    total: usize,
    used: usize,
    level: usize,
    mac_address: String,
    imported: Vec<String>,
}

impl BaseFile {
    pub fn new(total: usize, used: usize, level: usize, mac_address: &str) -> Self {
        Self { total, used, level, mac_address: mac_address.to_string(), imported: vec![] }
    }

    pub fn total(&self) -> usize {
        self.total
    }
    pub fn used(&self) -> usize {
        self.used
    }
    pub fn level(&self) -> usize {
        self.level
    }
    pub fn set_total(&mut self, total: usize) {
        self.total = total;
    }
    pub fn set_used(&mut self, used: usize) {
        self.used = used;
    }
    pub fn set_level(&mut self, level: usize) {
        self.level = level;
    }
    pub fn mac_address(&self) -> &str {
        &self.mac_address
    }
    pub fn set_mac_address(&mut self, mac_address: String) {
        self.mac_address = mac_address;
    }
    pub fn exchange(&mut self, other: Self) {
        self.level = other.level;
        self.total = other.total;
        self.used = other.used;
        self.mac_address = other.mac_address;
        self.imported = other.imported;
    }
    pub fn imported(&mut self) -> &mut Vec<String> {
        &mut self.imported
    }
}

#[derive(Serialize, Deserialize, Debug, Default, Clone)]
pub struct ImportSecret {
    time: usize,
    level: usize,
    mac_address: String,
    timestamp: i64,
}

impl ImportSecret {
    pub fn new(time: usize, level: usize, mac_address: &str) -> Self {
        ImportSecret { time, level, mac_address: mac_address.to_string(), timestamp: Local::now().timestamp() }
    }
    pub fn time(&self) -> usize {
        self.time
    }
    pub fn level(&self) -> usize {
        self.level
    }
    pub fn set_time(&mut self, time: usize) {
        self.time = time;
    }
    pub fn set_level(&mut self, level: usize) {
        self.level = level;
    }
    pub fn timestamp(&self) -> i64 {
        self.timestamp
    }
    pub fn set_timestamp(&mut self, timestamp: i64) {
        self.timestamp = timestamp;
    }
    pub fn set_mac_address(&mut self, mac_address: String) {
        self.mac_address = mac_address;
    }
    pub fn mac_address(&self) -> &str {
        &self.mac_address
    }
    pub fn to_json(&self) -> String {
        serde_json::to_string(&self).unwrap()
    }
}