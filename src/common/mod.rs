pub mod base_file;
pub mod err;

use serde::{Serialize, Deserialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct JsonResult {
    success: bool,
    code: usize,
    message: String,
}

impl JsonResult {
    pub fn success() -> Self {
        Self {
            success: true,
            code: 200,
            message: "success".to_string(),
        }
    }

    pub fn failed(code: usize, message: &str) -> Self {
        Self {
            success: false,
            code,
            message: message.to_string(),
        }
    }
}

impl JsonResult {
    pub fn to_json(&self) -> String {
        serde_json::to_string(&self).unwrap()
    }
}

#[test]
fn test() {}