use std::time::Duration;
use std::thread;

pub fn start<F>(init_sec: usize, sleep_sec: u64, func: F)
    where F: Fn(usize) + Send + Copy + Sync + 'static
{
    thread::spawn(move || {
        let mut current = init_sec;
        loop {
            thread::spawn(move || func(current));
            thread::sleep(Duration::new(sleep_sec, 0));
            current += sleep_sec as usize;
        }
    });
}

#[test]
fn test_start() {
    start(10, 10, |t| { println!("{}", t) });
    loop {}
}