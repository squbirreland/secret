use crate::base::BQ;
use crate::base;
use crate::common::base_file::BaseFile;
use std::process;

pub fn panic_(msg: String) -> ! {
    base::current_base().exchange(BaseFile::default());
    BQ.push(false);
    println!("{}", msg);
    process::abort()
}