use std::fs;
use std::path::Path;
use fast_log::consts::LogSize;
use fast_log::plugin::file_split::RollingType;
use log::Level;

pub fn init_log(debug: bool) {
    let path = Path::new("logs/");
    if !path.exists() {
        fs::create_dir("logs/").unwrap();
    }
    let level = if debug { Level::Debug } else { Level::Info };
    fast_log::init_split_log(
        "logs/",
        1000,
        LogSize::MB(1),
        false,
        RollingType::All,
        level,
        None,
        true,
    ).unwrap();
}