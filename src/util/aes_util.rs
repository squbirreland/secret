use aes_gcm::{Aes256Gcm, Key, Nonce};
use aes_gcm::aead::{Aead, NewAead};


pub fn encrypt(key: &[u8; 32], salt: &[u8; 12], payload: &[u8]) -> Vec<u8> {
    let cipher = Aes256Gcm::new(Key::from_slice(key));
    cipher.encrypt(Nonce::from_slice(salt), payload)
        .expect("encryption failure!")
}

pub fn decrypt(key: &[u8; 32], salt: &[u8; 12], secret: &[u8]) -> Vec<u8> {
    let cipher = Aes256Gcm::new(Key::from_slice(key));
    cipher.decrypt(Nonce::from_slice(salt), secret)
        .expect("decryption failure!")
}
