use std::error::Error;

pub fn rand_u8x12() -> [u8; 12] {
    let mut r = [0u8; 12];
    for ru8 in &mut r {
        *ru8 = rand::random::<u8>();
    }
    r
}

pub fn vu8_hex(vu8: &[u8]) -> String {
    let mut result = String::new();
    for x in vu8 {
        let hex = format!("{:x}", x);
        match hex.len() {
            3 => { result.push_str(&hex); }
            2 => { result.push_str(&format!("0{}", hex)); }
            1 => { result.push_str(&format!("00{}", hex)); }
            _ => {}
        }
    }
    result
}

pub fn hex_vu8(hex: &str) -> Result<Vec<u8>, Box<dyn Error>> {
    let mut result = vec![];
    if hex.len() < 3 { return Ok(result); }
    let mut start = 0;
    while start + 3 <= hex.len() {
        let a = &hex[start..start + 3];
        let ru8 = u8::from_str_radix(a, 16)?;
        result.push(ru8);
        start += 3;
    }
    Ok(result)
}

#[test]
fn test_rand_u8x12() {
    let v = rand_u8x12();
    println!("{:?}", v);
}

#[test]
fn hex_vu8_test() {
    let v = hex_vu8("0690b600604d0f60990cf0d80820710770c30f70560bf0050b60b80ce0f6");
    println!("{:?}", v);
}