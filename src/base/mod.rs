use std::sync::{Mutex, MutexGuard};

use lazy_static::lazy_static;

use crate::common::base_file::BaseFile;
use crate::util::blocking::BlockingQueue;

pub static KEY: [u8; 32] = *b"ecloud squbirreland encrypt key.";
pub static BASE_FILE: &str = "secret.base";

lazy_static! {
    pub static ref CURRENT_BASE : Mutex<BaseFile> = Mutex::new(BaseFile::default());
    pub static ref MAC_ADDRESS : String = mac_address::get_mac_address().unwrap().unwrap().to_string();
    pub static ref BQ : BlockingQueue<bool> = BlockingQueue::<bool>::default();
}

pub fn current_base() -> MutexGuard<'static, BaseFile> {
    CURRENT_BASE.lock().unwrap()
}