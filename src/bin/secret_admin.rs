use std::env;
use std::fs;
use secret::server::aes_server;
use std::str::FromStr;
use secret::base;

/// 用于生成盐与加密文本 交付给客户通过程序内的私钥与发送的盐解密文本
fn main() {
    let args: Vec<String> = env::args().collect();
    if args.is_empty() {
        println!("{}", help());
        return;
    }
    let order = &args[1];
    let result = match order.as_str() {
        "delay" => {
            let (payload, to_file) = match args[2].as_str() {
                "-file" => { (&args[3], true) }
                &_ => { (&args[2], false) }
            };
            let (day, level, mac_address) = day_level(payload);
            let result = aes_server::create_import_secret(day, level, &mac_address);
            if to_file { fs::write("./secret.lic", &result).unwrap(); }
            result
        }
        "init" => {
            let (day, level, mac_address) = day_level(&args[2]);
            aes_server::init_base(day, level, &mac_address);
            format!("success create file `{}`", base::BASE_FILE)
        }
        &_ => { help() }
    };
    println!("{}", result);
}

pub fn day_level(payload: &str) -> (usize, usize, String) {
    let mut split = payload.split('-');
    let d = split.next().unwrap();
    let l = split.next().unwrap();
    let m = split.next().unwrap();
    let day = usize::from_str(d).unwrap();
    let level = usize::from_str(l).unwrap();
    (day, level, m.to_string())
}

pub fn help() -> String {
    " orders :\n\
            ~ delay [day]-[level]-[mac_address]\n\
            ~ delay -file [day]-[level]-[mac_address]\n\
            ~ init [day]-[level]-[mac_address]\n\
            "
        .to_string()
}