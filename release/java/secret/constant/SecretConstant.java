package com.ruoyi.project.secret.constant;

/**
 * @author Squbi
 */

public enum SecretConstant {

    //请求地址
    GET_SECRET("/secret"),
    POST_SECRET("/secret");

    private final String value;

    SecretConstant(String value) {
        this.value = value;
    }

    public String toAddress(String prev) {
        return prev + value;
    }
}
