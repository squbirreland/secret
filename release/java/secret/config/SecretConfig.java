package com.ruoyi.project.secret.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * @author Squbi
 */
@Component
public class SecretConfig {

    @Value("${secret.ip}")
    private String ip;
    @Value("${secret.port}")
    private int port;

    public String getSecretAddress() {
        return "http://" + ip + ":" + port;
    }
}
