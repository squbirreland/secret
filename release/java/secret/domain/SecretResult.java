package com.ruoyi.project.secret.domain;

import lombok.Data;

/**
 * Secret服务的返回结果
 *
 * @author Squbi
 */
@Data
public class SecretResult {
    private boolean success;
    private int code;
    private String message;
}
