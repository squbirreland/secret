package com.ruoyi.project.secret.controller;

import com.ruoyi.project.secret.domain.SecretResult;
import com.ruoyi.project.secret.service.SecretServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SecretController {
    @Autowired
    private SecretServer secretServer;

    @PostMapping("/secret/{keyCode}")
    public SecretResult pushSecret(@PathVariable String keyCode) {
        return secretServer.pushCode(keyCode);
    }

    @GetMapping("/secret/info")
    public String getSecret() {
        return secretServer.leftTime();
    }
}
