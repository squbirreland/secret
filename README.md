## SECRET

#### 本地验权认证服务

----

### 使用

    编译后有 secret 和 secret_admin 两个可执行文件
    注意!!! 其中secret_admin不可交予用户

    在release包中有已编译好的linux和win版本执行文件
    以及java对接可能用到的类与方法

1. 初始化 \
   secret服务在第一次启动前 必须确保相同目录下有secret.base文件\
   而该文件由admin根据其服务所运行的mac地址生成\
   执行 `secret_admin init [day]-[leve]-[mac_address]` 即可\
   其中\
   `day` 为初始给予的使用时长\
   `level` 为逻辑上的权限等级 跟本secret服务无关\
   `mac_address` 为 secret运行所在的机器的mac地址

2. 运行 \
   secret服务默认启动端口为`7789`\
   可通过`./secret 8888`这种方式指定其端口\
   其中有两个接口 \
   `GET \secret` 为获取当前信息 \
   `POST \secret` 为服务提供认证码
    - 参数 body json
   ```json
   {"secret":"***认证码***"}
   ```
4. 认证码 \
   认证码由admin根据mac_address生成 包含增加的时间和等级信息\
   其命令 `secret_admin delay [day]-[level]-[mac_address]`\
   生成后交予服务 通过`POST \secret`进行认证并修改\
   已经使用过的认证码无法重复使用

5. 异常 \
   对于特殊的异常情况 主要集中在文件读取与解析环节\
   一旦发生则会使用内部封装的panic_ 停止当前web容器与主程序

----

### 规范

返回信息

```rust
/// POST请求增时时的返回
pub struct JsonResult {
    //是否成功
    success: bool,
    //返回码
    code: usize,
    //信息
    message: String,
}

/// GET获取信息时的返回
pub struct BaseFile {
    //所有时长 单位秒
    total: usize,
    //已用时长 单位秒
    used: usize,
    //逻辑权限等级
    level: usize,
    //绑定的mac地址
    mac_address: String,
    //已使用过的认证码
    imported: Vec<String>,
}
```

----

### 性能

程序内部有个计时器\
该计时器每10s会执行一个定时任务\
以同步当前时间和状态到secret.base文件\
但是该任务的执行效率受到机器本身性能\
以及加时次数 (加密文件大小) 的影响

|加时次数|定时任务消耗时间(ms)|
|---|---|
|4|1|
|8|1|
|12|2|
|16|2|
|20|3|
|24|3|

    粗略估算每8次加时 计时器执行时间会多1ms
    也就是8万次加时时 计时器会达到理论上限 执行时长达到10s
    加时的最小单位为天 如果每次仅加时1天 理论上该程序可支撑219年
    如果正常加时 每次加时30天~365天 理论上可以支撑6575~80000年
